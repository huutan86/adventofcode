"""Solves day 21 challenge.
--- Day 21: Allergen Assessment ---
You reach the train's last stop and the closest you can get to your vacation island without getting wet. There aren't
even any boats here, but nothing can stop you now: you build a raft. You just need a few days' worth of food for your
journey.

You don't speak the local language, so you can't read any ingredients lists. However, sometimes, allergens are listed
in a language you do understand. You should be able to use this information to determine which ingredient contains which
allergen and work out which foods are safe to take with you on your trip.

You start by compiling a list of foods (your puzzle input), one food per line. Each line includes that food's
ingredients list followed by some or all of the allergens the food contains.

Each allergen is found in exactly one ingredient. Each ingredient contains zero or one allergen. Allergens aren't always
marked; when they're listed (as in (contains nuts, shellfish) after an ingredients list), the ingredient that contains
each listed allergen will be somewhere in the corresponding ingredients list. However, even if an allergen isn't listed,
the ingredient that contains that allergen could still be present: maybe they forgot to label it, or maybe it was
labeled in a language you don't know.

For example, consider the following list of foods:

mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)

The first food in the list has four ingredients (written in a language you don't understand): mxmxvkd, kfcds, sqjhc, and
nhms. While the food might contain other allergens, a few allergens the food definitely contains are listed afterward:
dairy and fish.

The first step is to determine which ingredients can't possibly contain any of the allergens in any food in your list.
In the above example, none of the ingredients kfcds, nhms, sbzzf, or trh can contain an allergen. Counting the number
of times any of these ingredients appear in any ingredients list produces 5: they all appear once each except sbzzf,
which appears twice.

Determine which ingredients cannot possibly contain any of the allergens in your list. How many times do any of those
ingredients appear?

--- Part Two ---
Now that you've isolated the inert ingredients, you should have enough information to figure out which ingredient
contains which allergen.

In the above example:

mxmxvkd contains dairy.
sqjhc contains fish.
fvjkl contains soy.
Arrange the ingredients alphabetically by their allergen and separate them by commas to produce your canonical dangerous
ingredient list. (There should not be any spaces in your canonical dangerous ingredient list.) In the above example,
this would be mxmxvkd,sqjhc,fvjkl.

Time to stock your raft with supplies. What is your canonical dangerous ingredient list?

Better solution: https://github.com/zecookiez/AdventOfCode2020/blob/main/day21_allergen_assessment.py
This is an example of constrain propgattion problem: https://en.wikipedia.org/wiki/Constraint_satisfaction
Another solution for part 2: use the maximum matching function in the bipartite graph.
https://www.geeksforgeeks.org/maximum-bipartite-matching/
https://campus.datacamp.com/courses/intermediate-network-analysis-in-python/bipartite-graphs-product-recommendation-systems?ex=4
"""
from pathlib import Path
from os.path import join
from absl import logging


def compute_possibility_of_allergen(ing, map_dict, all_rules):
    """Returns a dictionary for the possibility of an allergen of an input `ing` ingredient.

    Rules for checking:
        An ingredient can't contain an allergen in 1 line if it does not appear in another line in which the allergen
        appears!!!
    """
    als = map_dict[ing]
    res = {}
    for al in als:
        # Extract all ingredients from rules that contains the current allergen
        ing_list = [rule[0] for rule in all_rules if al in rule[1]]
        res[al] = all(ing in item for item in ing_list)
    return res


def check_impossible(ing, map_dict, all_rules) -> bool:
    """Returns True if an ingredient can't contain any allergens."""
    res = compute_possibility_of_allergen(ing, map_dict, all_rules)
    return all(not item for item in res.values())


def day21():
    map_dict = {}
    lines = [line[:-2] for line in open(join(Path(__file__).parent, "data", "day21_input.txt"), "r").readlines()]
    all_rules = []
    all_ingredient_lines = []
    all_ingredients = set()
    for line in lines:
        ings, allergens = line.split(" (contains ")
        ings = ings.split(" ")
        all_ingredient_lines.append(ings)
        allergens = allergens.split(", ")

        for ing in ings:
            all_ingredients.add(ing)

        all_rules.append((ings, allergens))

        # Construct a dictionary of a possible allergens for each ingredients
        for item in ings:
            if map_dict.get(item) is None:
                map_dict[item] = allergens.copy()
            else:
                for al in allergens:
                    map_dict[item].append(al)

    impossible_list = [item for item in all_ingredients if check_impossible(item, map_dict, all_rules)]
    res = sum(1 for line in all_ingredient_lines for item in impossible_list if item in line)
    logging.info(f"Part 1 res = {res}")

    possible_list = [item for item in all_ingredients if item not in impossible_list]
    all_rules_simplified = []
    for rule in all_rules:
        ingredient_list = rule[0]
        for item in impossible_list:
            if item in ingredient_list:
                ingredient_list.remove(item)
        all_rules_simplified.append((ingredient_list, rule[1]))

    for item in impossible_list:
        map_dict.pop(item)

    ing_to_al_map = {}
    for ing in possible_list:
        ing_to_al_map[ing] = compute_possibility_of_allergen(ing, map_dict, all_rules_simplified)

    while True:
        if all(len(ing_to_al_map[ing].values()) == 1 for ing in ing_to_al_map):
            break

        # Handle the case where it is clear to specify an ingredient-allergen relation.
        for ing in possible_list:
            if sum(ing_to_al_map[ing].values()) == 1:
                decided_al = next(item for item in ing_to_al_map[ing] if ing_to_al_map[ing][item])
                ing_to_al_map[ing] = {decided_al: True}
                # Remove the allergen from consideration of other ings
                for other_ing in possible_list:
                    if other_ing != ing:
                        if decided_al in ing_to_al_map[other_ing]:
                            ing_to_al_map[other_ing].pop(decided_al)

    final_map = [(ing, list(ing_to_al_map[ing].keys())[0]) for ing in ing_to_al_map]
    sorted_map = sorted(final_map, key=lambda x: x[1])
    logging.info(f"Part2 = {''.join(item[0]+',' for item in sorted_map)[:-1]}")








