"""Solves day 19 challenge.


--- Day 19: Monster Messages ---
You land in an airport surrounded by dense forest. As you walk to your high-speed train, the Elves at the Mythical
Information Bureau contact you again. They think their satellite has collected an image of a sea monster! Unfortunately,
the connection to the satellite is having problems, and many of the messages sent back from the satellite have been
corrupted.

They sent you a list of the rules valid messages should obey and a list of received messages they've collected so far
(your puzzle input).

The rules for valid messages (the top part of your puzzle input) are numbered and build upon each other. For example:

0: 1 2
1: "a"
2: 1 3 | 3 1
3: "b"
Some rules, like 3: "b", simply match a single character (in this case, b).

The remaining rules list the sub-rules that must be followed; for example, the rule 0: 1 2 means that to match rule 0, t
he text being checked must match rule 1, and the text after the part that matched rule 1 must then match rule 2.

Some of the rules have multiple lists of sub-rules separated by a pipe (|). This means that at least one list of
sub-rules must match. (The ones that match might be different each time the rule is encountered.) For example, the rule
2: 1 3 | 3 1 means that to match rule 2, the text being checked must match rule 1 followed by rule 3 or it must match
rule 3 followed by rule 1.

Fortunately, there are no loops in the rules, so the list of possible matches will be finite. Since rule 1 matches a
and rule 3 matches b, rule 2 matches either ab or ba. Therefore, rule 0 matches aab or aba.

Here's a more interesting example:

0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"
Here, because rule 4 matches a and rule 5 matches b, rule 2 matches two letters that are the same (aa or bb), and rule 3
matches two letters that are different (ab or ba).

Since rule 1 matches rules 2 and 3 once each in either order, it must match two pairs of letters, one pair with matching
letters and one pair with different letters. This leaves eight possibilities: aaab, aaba, bbab, bbba, abaa, abbb, baaa,
or babb.

Rule 0, therefore, matches a (rule 4), then any of the eight options from rule 1, then b (rule 5): aaaabb, aaabab,
abbabb, abbbab, aabaab, aabbbb, abaaab, or ababbb.

The received messages (the bottom part of your puzzle input) need to be checked against the rules so you can determine
which are valid and which are corrupted. Including the rules and the messages together, this might look like:

0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb
Your goal is to determine the number of messages that completely match rule 0. In the above example, ababbb and abbbab
match, but bababa, aaabbb, and aaaabbb do not, producing the answer 2. The whole message must match all of rule 0;
there can't be extra unmatched characters in the message. (For example, aaaabbb might appear to match rule 0 above, but
it has an extra unmatched b on the end.)

How many messages completely match rule 0?

--- Part Two ---
As you look over the list of messages, you realize your matching rules aren't quite right. To fix them, completely
replace rules 8: 42 and 11: 42 31 with the following:

8: 42 | 42 8
11: 42 31 | 42 11 31
This small change has a big impact: now, the rules do contain loops, and the list of messages they could hypothetically
match is infinite. You'll need to determine how these changes affect which messages are valid.

Fortunately, many of the rules are unaffected by this change; it might help to start by looking at which rules always
match the same set of values and how those rules (especially rules 42 and 31) are used by the new versions of rules 8
 and 11.

(Remember, you only need to handle the rules you have; building a solution that could handle any hypothetical combination
of rules would be significantly more difficult.)

For example:

42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
Without updating rules 8 and 11, these rules only match three messages: bbabbbbaabaabba, ababaaaaaabaaab, and ababaaaaabbbaba.

However, after updating rules 8 and 11, a total of 12 messages match:

bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
After updating rules 8 and 11, how many messages completely match rule 0?

### Example of regular expression puzzle, we need to generate a regex pattern for this!!! ####
"""
from pathlib import Path
from os.path import join
from absl import logging
from typing import List
from typing import Tuple
from typing import Set
import re


def parse_rule(line: str) -> List[int]:
    if line[0] == '"':
        return line[1]  # Take all the pattern between the two " "'s
    return line


def expand_tree(cur_path: Tuple[int], rules, memoization=set(), visited_nodes=set()) -> Set[Tuple[int]]:
    """(Depricated) Silly approach, try to reconstruct the whole space and compare with the result for a match!!!

    Don't run this. It is the most stupid approach because it takes lots of time on the input set."""
    memoization.add(cur_path)
    for idx, item in enumerate(cur_path):
        # Skip this is a terminal node
        if isinstance(rules[item], str):
            continue
        subpaths = rules[item]
        # Replace the current node with all the new paths.
        new_paths = [(*cur_path[:idx], *subpath, *cur_path[idx+1:]) for subpath in subpaths]
        if cur_path in memoization:
            memoization.remove(cur_path)
        for new_path in new_paths:
            memoization.add(new_path)
            if new_path not in visited_nodes:
                expand_tree(new_path, rules, memoization, visited_nodes)
    visited_nodes.add(cur_path)
    return memoization


def create_regex(rule_idx: int, rules, memo={}, depth=1) -> str:
    # Construct a regex for a rule indexed by `rule_idx` and all the rules below it.

    if depth == 0:
        # This recursive when there is looping is still not yet correct.
        return ''

    if rules[rule_idx].isalpha():
        memo[rule_idx] = rules[rule_idx]
        return memo[rule_idx]

    if len(rules[rule_idx]) == 1:
        out_pattern = ["(", create_regex(int(rules[rule_idx]), rules, memo), ")"]
        memo[rule_idx] = ''.join(out_pattern)
        return memo[rule_idx]

    out_pattern = ["(", "("]  # Prevent the blowing of the nested expression when joining.
    for item in rules[rule_idx].split(' '):
        if item == '|':
            out_pattern.append(")")
            out_pattern.append("|")  # To select one of the options.
            out_pattern.append("(")
        else:
            if item in memo:
                out_pattern.append(memo[item])
            else:
                if int(item) == rule_idx:
                    out_pattern.append(create_regex(int(item), rules, memo, depth=depth-1))
                else:
                    out_pattern.append(create_regex(int(item), rules, memo))

    out_pattern.append(")")
    out_pattern.append(")")
    memo[rule_idx] = ''.join(out_pattern)
    return memo[rule_idx]


def day19():
    data = [line[:-1] for line in open(join(Path(__file__).parent, "data", "day19_input.txt")).readlines()]
    rule_temp = [line.split(": ") for line in data[:133]]  # 133, 31 The later is for the toy example.
    rules = {int(line[0]): parse_rule(line[1]) for line in rule_temp}

    # 1st attempt: reconstruct all the combination and matches -> Failed because it takes forever to create the space!

    # 2nd attempt: create a regex for matching.
    messages = data[134:]  #134, 32

    pattern = create_regex(0, rules)
    logging.info(f'Part1 {sum(1 for line in messages if re.fullmatch(pattern, line))}')

    # part 2:
    rules[8] = "42 | 42 42 | 42 42 42 | 42 42 42 42 | 42 42 42 42 42"   # Factorized from 42 | 42 8 after 5 time recur.
    rules[11] = "42 31 | 42 42 31 31 | 42 42 42 31 31 31 | 42 42 42 42 31 31 31 31 | 42 42 42 42 42 31 31 31 31 31"
    # Factorized from 42 31 | 42 11 31

    pattern = create_regex(0, rules, memo={}, depth=5)
    logging.info(f'Part2 {sum(1 for line in messages if re.fullmatch(pattern, line))}')
