"""Solves day 23 challenge.

--- Day 23: Crab Cups ---
The small crab challenges you to a game! The crab is going to mix up some cups, and you have to predict where they'll end up.

The cups will be arranged in a circle and labeled clockwise (your puzzle input). For example, if your labeling were
32415, there would be five cups in the circle; going clockwise around the circle from the first cup, the cups would be
labeled 3, 2, 4, 1, 5, and then back to 3 again.

Before the crab starts, it will designate the first cup in your list as the current cup. The crab is then going to do
100 moves.

Each move, the crab does the following actions:

The crab picks up the three cups that are immediately clockwise of the current cup. They are removed from the circle;
 cup spacing is adjusted as necessary to maintain the circle.
The crab selects a destination cup: the cup with a label equal to the current cup's label minus one. If this would
select one of the cups that was just picked up, the crab will keep subtracting one until it finds a cup that wasn't just
 picked up. If at any point in this process the value goes below the lowest value on any cup's label, it wraps around to
the highest value on any cup's label instead.
The crab places the cups it just picked up so that they are immediately clockwise of the destination cup. They keep the
same order as when they were picked up.
The crab selects a new current cup: the cup which is immediately clockwise of the current cup.
For example, suppose your cup labeling were 389125467. If the crab were to do merely 10 moves, the following changes
would occur:

-- move 1 --
cups: (3) 8  9  1  2  5  4  6  7
pick up: 8, 9, 1
destination: 2

-- move 2 --
cups:  3 (2) 8  9  1  5  4  6  7
pick up: 8, 9, 1
destination: 7

-- move 3 --
cups:  3  2 (5) 4  6  7  8  9  1
pick up: 4, 6, 7
destination: 3

-- move 4 --
cups:  7  2  5 (8) 9  1  3  4  6
pick up: 9, 1, 3
destination: 7

-- move 5 --
cups:  3  2  5  8 (4) 6  7  9  1
pick up: 6, 7, 9
destination: 3

-- move 6 --
cups:  9  2  5  8  4 (1) 3  6  7
pick up: 3, 6, 7
destination: 9

-- move 7 --
cups:  7  2  5  8  4  1 (9) 3  6
pick up: 3, 6, 7
destination: 8

-- move 8 --
cups:  8  3  6  7  4  1  9 (2) 5
pick up: 5, 8, 3
destination: 1

-- move 9 --
cups:  7  4  1  5  8  3  9  2 (6)
pick up: 7, 4, 1
destination: 5

-- move 10 --
cups: (5) 7  4  1  8  3  9  2  6
pick up: 7, 4, 1
destination: 3

-- final --
cups:  5 (8) 3  7  4  1  9  2  6
In the above example, the cups' values are the labels as they appear moving clockwise around the circle;
the current cup is marked with ( ).

After the crab is done, what order will the cups be in? Starting after the cup labeled 1, collect the other cups' labels
clockwise into a single string with no extra characters; each number except 1 should appear exactly once. In the above
example, after 10 moves, the cups clockwise from 1 are labeled 9, 2, 6, 5, and so on, producing 92658374. If the crab
were to complete all 100 moves, the order after cup 1 would be 67384529.

Using your labeling, simulate 100 moves. What are the labels on the cups after cup 1?

--- Part Two ---
Due to what you can only assume is a mistranslation (you're not exactly fluent in Crab), you are quite surprised when
the crab starts arranging many cups in a circle on your raft - one million (1000000) in total.

Your labeling is still correct for the first few cups; after that, the remaining cups are just numbered in an increasing
 fashion starting from the number after the highest number in your list and proceeding one by one until one million is
 reached. (For example, if your labeling were 54321, the cups would be numbered 5, 4, 3, 2, 1, and then start counting
 up from 6 until one million is reached.) In this way, every number from one through one million is used exactly once.

After discovering where you made the mistake in translating Crab Numbers, you realize the small crab isn't going to do
merely 100 moves; the crab is going to do ten million (10000000) moves!

The crab is going to hide your stars - one each - under the two cups that will end up immediately clockwise of cup 1.
You can have them if you predict what the labels on those cups will be when the crab is finished.

In the above example (389125467), this would be 934001 and then 159792; multiplying these together produces 149245887792.

Determine which two cups will end up immediately clockwise of cup 1. What do you get if you multiply their labels together?


"""
from pathlib import Path
from os.path import join
from util import timing
from absl import logging


def day23():
    input = [7, 9, 2, 8, 4, 5, 1, 3, 6]
    #input = [3, 8, 9, 1, 2, 5, 4, 6, 7]

    def move(input, moves):
        """First version of the move, using a list very slow!!!"""
        move = 0
        max_val = max(input)
        min_val = min(input)
        current_idx = 0
        current_cup = input[current_idx]
        while move < moves:
            current_idx = next(i for i in range(len(input)) if input[i] ==current_cup)
            pickup = [input[v % len(input)] for v in range(current_idx + 1, current_idx + 4)]
            pickup_idx = [v % len(input) for v in range(current_idx + 1, current_idx + 4)]
            # Find the des
            des = current_cup - 1
            while True:
                if des < min_val:
                    des = max_val
                if des not in pickup:
                    break
                else:
                    des -= 1
            # Update the new current for the next iteration before putting it back
            current_cup = input[(current_idx + 4) % len(input)]

            # Remove the pick up, adjust the circle
            adjusted_input = [input[v % len(input)] for v in range(len(input)) if v not in pickup_idx]
            input = adjusted_input

            des_idx = next(i for i in range(len(input)) if input[i] == des)

            # Place it.
            input[des_idx + 1 : des_idx + 1] = pickup

            # Just rotate the list to start with 1
            one_idx = next(i for i in range(len(input)) if input[i] == 1)
            input = [input[i % len(input)] for i in range(one_idx, one_idx + len(input))]

            move += 1
        return input

    def move2(neigh_dict, input, moves):
        """2nd version of the move using the linked list. Much faster since we don't need to update many elements."""
        move = 0
        max_val = max(neigh_dict.keys())
        min_val = min(neigh_dict.keys())
        current_cup = input[0]
        while move < moves:
            pickup = [neigh_dict[current_cup],
                      neigh_dict[neigh_dict[current_cup]],
                      neigh_dict[neigh_dict[neigh_dict[current_cup]]]]
            # Find the des
            des = current_cup - 1
            while True:
                if des < min_val:
                    des = max_val
                if des not in pickup:
                    break
                else:
                    des -= 1

            #logging.info(f"Move = {move + 1}, current = {current_cup}, pick up = {pickup}, destimation = {des}")
            # Update the new current for the next iteration before putting it back
            next_current_cup = neigh_dict[neigh_dict[neigh_dict[neigh_dict[current_cup]]]]

            # Remove the pick up, adjust the circle
            neigh_dict[current_cup] = next_current_cup

            # Link the last pickup element
            neigh_dict[pickup[-1]] = neigh_dict[des]

            # Link the des against it
            neigh_dict[des] = pickup[0]

            current_cup = next_current_cup
            move += 1
        return neigh_dict

    # --- Part 1 ---
    out_str = ''
    item = 1
    next_item = None
    neigh_dict = {input[i]: input[(i + 1) % len(input)] for i in range(len(input))}
    neigh_dict = move2(neigh_dict, input, moves=100)
    while True:
        next_item = neigh_dict[item]
        if next_item != 1:
            out_str += str(next_item)
        else:
            break
        item = next_item
    logging.info(f"Part 1 res = {out_str}")

    # --- Part 2 ---
    input2 = input.copy()
    input2.extend(range(max(input) + 1, 1000001))
    neigh_dict = {input2[i]: input2[(i + 1) % len(input2)] for i in range(len(input2))}
    out = move2(neigh_dict, input2, moves=10000000)
    logging.info(f"Part 2 res = {out[1] * out[out[1]]}")


