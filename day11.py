"""Solves day 11 challenges.

--- Day 11: Seating System ---
Your plane lands with plenty of time to spare. The final leg of your journey is a ferry that goes directly to the
tropical island where you can finally start your vacation. As you reach the waiting area to board the ferry, you realize
you're so early, nobody else has even arrived yet!

By modeling the process people use to choose (or abandon) their seat in the waiting area, you're pretty sure you can
predict the best place to sit. You make a quick map of the seat layout (your puzzle input).

The seat layout fits neatly on a grid. Each position is either floor (.), an empty seat (L), or an occupied seat (#).
For example, the initial seat layout might look like this:

L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

Now, you just need to model the people who will be arriving shortly. Fortunately, people are entirely predictable and
always follow a simple set of rules. All decisions are based on the number of occupied seats adjacent to a given seat
(one of the eight positions immediately up, down, left, right, or diagonal from the seat). The following rules are
applied to every seat simultaneously:

If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
Otherwise, the seat's state does not change.
Floor (.) never changes; seats don't move, and nobody sits on the floor.

After one round of these rules, every seat in the example layout becomes occupied:

#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
After a second round, the seats with four or more occupied adjacent seats become empty again:

#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##
This process continues for three more rounds:

#.##.L#.##
#L###LL.L#
L.#.#..#..
#L##.##.L#
#.##.LL.LL
#.###L#.##
..#.#.....
#L######L#
#.LL###L.L
#.#L###.##
#.#L.L#.##
#LLL#LL.L#
L.L.L..#..
#LLL.##.L#
#.LL.LL.LL
#.LL#L#.##
..L.L.....
#L#LLLL#L#
#.LLLLLL.L
#.#L#L#.##
#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##
At this point, something interesting happens: the chaos stabilizes and further applications of these rules cause no
seats to change state! Once people stop moving around, you count 37 occupied seats.

Simulate your seating area by applying the seating rules repeatedly until no seats change state.
How many seats end up occupied?

--- Part Two ---
As soon as people start to arrive, you realize your mistake. People don't just care about adjacent seats - they care
about the first seat they can see in each of those eight directions!

Now, instead of considering just the eight immediately adjacent seats, consider the first seat in each of those eight
directions. For example, the empty seat below would see eight occupied seats:

.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#.....
The leftmost empty seat below would only see one empty seat, but cannot see any of the occupied ones:

.............
.L.L.#.#.#.#.
.............
The empty seat below would see no occupied seats:

.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.
Also, people seem to be more tolerant than you expected: it now takes five or more visible occupied seats for an
occupied seat to become empty (rather than four or more from the previous rules). The other rules still apply: empty
seats that see no occupied seats become occupied, seats matching no rule don't change, and floor never changes.

Given the same starting layout as above, these new rules cause the seating area to shift around as follows:

L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#
#.L#.##.L#
#L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
#.#####.#L
..#.#.....
LLL####LL#
#.L#####.L
#.L####.L#
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
#.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLL#.L
#.L#LL#.L#
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
#.L####.LL
..#.#.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
#.LLLL#.LL
..#.L.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
Again, at this point, people stop shifting around and the seating area reaches equilibrium. Once this occurs, you count
 26 occupied seats.

Given the new visibility method and the rule change for occupied seats becoming empty, once equilibrium is reached, how
many seats end up occupied?

"""
from pathlib import Path
from absl import logging
from os.path import join
from util import timing
from typing import List
from itertools import product
import copy

# Notes to speed this up
# 1. Pull out the neighbor computation for the seats only.
# 2. Don't use deep copy in each iteration.
# See Victor solution at https://github.com/victorminden/advent_of_code/blob/master/advent_of_code/day11/solution.py
# (<1 seconds)

def day11():
    data = [list(line[:-1]) for line in open(join(Path(__file__).parent, "data", "day11_input.txt")).readlines()]
    with timing("Part 1"):
        _part1(data)
    with timing("Part2"):
        _part2(data)


def _is_valid(r, c, nr, nc) -> bool:
    return 0 <= r < nr and 0 <= c <nc


def _count_adjecent(data: List[List[int]], row, col, num_rows, num_cols, max_range) -> int:
    def check_occupy(x_fact, y_fact) -> bool:
        for i in range(1, max_range + 1):
            new_row = y_fact * i + row
            new_col = x_fact * i + col
            if not _is_valid(new_row, new_col, num_rows, num_cols):
                return False
            if data[new_row][new_col] == '#':
                return True
            if data[new_row][new_col] == "L":
                return False
        return False
    return sum(check_occupy(i, j) for i, j in product(range(-1, 2), range(-1, 2)) if (i, j) != (0, 0))


def sim(data: List[List[int]], org_data: List[List[int]], tolerance, search_limit):
    num_rows = len(data)
    num_cols = len(data[0])
    for row, col in product(range(num_rows), range(num_cols)):
        if org_data[row][col] == 'L' and _count_adjecent(org_data, row, col, num_rows, num_cols,
                                                     max_range=search_limit) == 0:
            data[row][col] = "#"
        if org_data[row][col] == '#' and _count_adjecent(org_data, row, col, num_rows, num_cols,
                                                     max_range=search_limit) >= tolerance:
            data[row][col] = "L"


def _part1(data):
    """Solves part 1 challenge."""
    data_part1 = copy.deepcopy(data)
    # Thanks to victor.minden to told to me to get rid of the deep copy here.
    data_out = copy.deepcopy(data)
    num_rows = len(data_part1)
    num_cols = len(data_part1[0])
    while True:
        sim(data_out, data_part1, 4, 1)
        sim(data_part1, data_out, 4, 1)
        if data_out == data_part1:
            break

    logging.info(f"Results "
                 f"{sum([data_part1[row][col] == '#' for row, col in product(range(num_rows), range(num_cols))])}")


def _part2(data):
    """Solves part 2 challenge."""
    data_part2 = copy.deepcopy(data)
    data_out = copy.deepcopy(data)
    num_rows = len(data_part2)
    num_cols = len(data_part2[0])
    while True:
        sim(data_out, data_part2, 5, max(num_rows, num_cols))
        sim(data_part2, data_out, 5, max(num_rows, num_cols))
        if data_part2 == data_out:
            break
    logging.info(f"Results "
                 f"{sum([data_part2[row][col] == '#' for row, col in product(range(num_rows), range(num_cols))])}")

