# Generates a utilizes for measure the timing of the function
# See https://blog.usejournal.com/how-to-create-your-own-timing-context-manager-in-python-a0e944b48cf8
from contextlib import contextmanager
from time import time
from absl import logging


@contextmanager
def timing(desc: str) -> None:
    start = time()
    yield
    logging.info(f"{desc} takes {time()-start:1.4f} seconds to complete")
