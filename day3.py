"""Solves the challenge for day 3.

--- Day 3: Toboggan Trajectory ---
With the toboggan login problems resolved, you set off toward the airport. While travel by toboggan might be easy,
it's certainly not safe: there's very minimal steering and the area is covered in trees. You'll need to see which angles
will take you near the fewest trees.
Due to the local geology, trees in this area only grow on exact integer coordinates in a grid. You make a map (your
puzzle input) of the open squares (.) and trees (#) you can see. For example:
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#

These aren't the only trees, though; due to something you read about once involving arboreal genetics and biome
stability, the same pattern repeats to the right many times:

..##.........##.........##.........##.........##.........##.......  --->
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
You start on the open square (.) in the top-left corner and need to reach the bottom (below the bottom-most row on your
map). The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers rational numbers);
start by counting all the trees you would encounter for the slope right 3, down 1:

From your starting position at the top-left, check the position that is right 3 and down 1. Then, check the position
that is right 3 and down 1 from there, and so on until you go past the bottom of the map.
The locations you'd check in the above example are marked here with O where there was an open square and X where there
was a tree:

..##.........##.........##.........##.........##.........##.......  --->
#..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........X.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...#X....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
In this example, traversing the map using this slope would cause you to encounter 7 trees.

Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many trees would you
encounter?


--- Part Two ---
Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal stop, after all.

Determine the number of trees you would encounter if, for each of the following slopes, you start at the top-left corner
and traverse the map all the way to the bottom:

Right 1, down 1.
Right 3, down 1. (This is the slope you already checked.)
Right 5, down 1.
Right 7, down 1.
Right 1, down 2.
In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied together, these produce
the answer 336.

What do you get if you multiply together the number of trees encountered on each of the listed slopes?

"""
from pathlib import Path
from os.path import join
from typing import List
from typing import Tuple
from absl import logging
from functools import reduce
from operator import mul


def day3():
    """Solves the day 3 challenge."""
    data = _load_day3_data()
    _part1(data)
    _part2(data)
    _part2_better_sol(data)

def _tree_count(data: List[str], step: Tuple[int, int] = (3, 1)) -> int:
    return sum((row % step[1] == 0 and line[(row * step[0] // step[1]) % len(line)] == '#' for row, line in
                enumerate(data)))


def _part1(data: List[str]) -> None:
    """Solves the challenge in part 1."""
    logging.info(f"Number of trees observed = {_tree_count(data)}")


def _part2(data: List[str]) -> None:
    """Solves the challenge in part 2"""
    temp =_tree_count(data, step=(1, 1)) * _tree_count(data, step=(3,1)) * _tree_count(data, step=(5,1)) *\
          _tree_count(data, step=(7, 1)) * _tree_count(data, step=(1,2))
    logging.info(f"Result = {temp}")


def _part2_better_sol(data: List[str]) -> None:
    """Better solution using the reduce function. See https://realpython.com/python-reduce-function/. From Victor!!!"""
    logging.info(f"Result = {reduce(mul, (_tree_count(data, step) for step in [(1,1), (3,1), (5,1), (7,1), (1, 2)]))}")


def _load_day3_data() -> List[str]:
    with open(join(Path(__file__).parent, "data", "day3_input.txt"), 'r') as file:
        return [line.strip() for line in file]
