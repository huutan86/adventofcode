from absl import flags
from absl import app
from absl import logging

import importlib
for day in range(1, 25):
    day_str = f"day{day}"
    # Add to the global name space
    globals()[day_str] = getattr(importlib.import_module(day_str), day_str)


flags.DEFINE_integer("day", None, "The number of day to solve the challenge")
flags.mark_flag_as_required("day")

FLAGS = flags.FLAGS


def main(argv):
    """Solves for the solution."""
    del argv
    logging.info(f"Solving challenge of day {FLAGS.day}")
    try:
        globals()[f'day{FLAGS.day}']()
    except Exception as e:
        raise ValueError(f"Can't call the function on day {FLAGS.day}")
        raise e


if __name__ == "__main__":
    app.run(main)