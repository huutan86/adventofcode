"""Solves day 16 challenge.

--- Day 16: Ticket Translation ---
As you're walking to yet another connecting flight, you realize that one of the legs of your re-routed trip coming up is
 on a high-speed train. However, the train ticket you were given is in a language you don't understand. You should
 probably figure out what it says before you get to the train station after the next flight.

Unfortunately, you can't actually read the words on the ticket. You can, however, read the numbers, and so you figure out
 the fields these tickets must have and the valid ranges for values in those fields.

You collect the rules for ticket fields, the numbers on your ticket, and the numbers on other nearby tickets for the same
 train service (via the airport security cameras) together into a single document you can reference (your puzzle input).

The rules for ticket fields specify a list of fields that exist somewhere on the ticket and the valid ranges of values
for each field. For example, a rule like class: 1-3 or 5-7 means that one of the fields in every ticket is named class
and can be any value in the ranges 1-3 or 5-7 (inclusive, such that 3 and 5 are both valid in this field, but 4 is not).

Each ticket is represented by a single line of comma-separated values. The values are the numbers on the ticket in the
order they appear; every ticket has the same format. For example, consider this ticket:

.--------------------------------------------------------.
| ????: 101    ?????: 102   ??????????: 103     ???: 104 |
|                                                        |
| ??: 301  ??: 302             ???????: 303      ??????? |
| ??: 401  ??: 402           ???? ????: 403    ????????? |
'--------------------------------------------------------'
Here, ? represents text in a language you don't understand. This ticket might be represented as 101,102,103,104,301,302,
303,401,402,403; of course, the actual train tickets you're looking at are much more complicated. In any case, you've
extracted just the numbers in such a way that the first number is always the same specific field, the second number is
always a different specific field, and so on - you just don't know what each position actually means!

Start by determining which tickets are completely invalid; these are tickets that contain values which aren't valid for
any field. Ignore your ticket for now.

For example, suppose you have the following notes:

class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
It doesn't matter which position corresponds to which field; you can identify invalid nearby tickets by considering only
 whether tickets contain values that are not valid for any field. In this example, the values on the first nearby ticket
are all valid for at least one field. This is not true of the other three nearby tickets: the values 4, 55, and 12 are
are not valid for any field. Adding together all of the invalid values produces your ticket scanning error rate: 4 + 55
+ 12 = 71.

Consider the validity of the nearby tickets you scanned. What is your ticket scanning error rate?

--- Part Two ---
Now that you've identified which tickets contain invalid values, discard those tickets entirely. Use the remaining valid
tickets to determine which field is which.

Using the valid ranges for each field, determine what order the fields appear on the tickets. The order is consistent
between all tickets: if seat is the third field, it is the third field on every ticket, including your ticket.

For example, suppose you have the following notes:

class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9
Based on the nearby tickets in the above example, the first position must be row, the second position must be class, and
 the third position must be seat; you can conclude that in your ticket, class is 12, row is 11, and seat is 13.

Once you work out which field is which, look for the six fields on your ticket that start with the word departure. What
do you get if you multiply those six values together?

Shorter sol: https://github.com/zecookiez/AdventOfCode2020/blob/main/day16_ticket_translation.py

See a scipy solution: https://github.com/fuglede/adventofcode/blob/master/2020/day16/solutions.py
"""

from pathlib import Path
from os.path import join
from absl import logging
from util import timing
import numpy as np
import re

def day16():
    # Use strip() is more preferred than [-1] to crop out the \n.
    data = [line.strip() for line in open(join(Path(__file__).parent, "data", "day16_input.txt"), "r").readlines()]
    rule_lines = list(map(lambda line: line.split(": "), data[:20]))

    # Using regular expression to find all the character and pass them to a list.
    rules = {line[0]: list(map(int, re.findall("\d+", line[1]))) for line in rule_lines}
    my_ticket = list(map(int, data[22].split(",")))
    nearby_tickets = list(list(map(int, line.split(","))) for line in data[25:])
    with timing("Solution"):
        solve(rules, nearby_tickets, my_ticket)


def find_rule(candidates, output_map={}):
    # Part 2 recursive search for the 1-1 mapping.
    if candidates == {}:
        return output_map
    count_dict = {col: len(field) for col, field in candidates.items()}
    good_key = next(i for i in count_dict if count_dict[i] == 1)

    # Memoization
    output_map[good_key] = candidates[good_key][0]

    # Remove it from the dictionary
    candidates.pop(good_key)

    # Remove the value from the dictionary values
    for k, v in candidates.items():
        if output_map[good_key] in v:
            candidates[k].remove(output_map[good_key])
    return find_rule(candidates, output_map)


def solve(rules, nearby_tks, me):
    # Part 1 challenge.
    res = sum(item for tk in nearby_tks for item in tk if not any((rule[0] <= item <= rule[1]) or
                                                                  (rule[2] <= item <= rule[3]) for
                                                                  rule in rules.values()))
    logging.info(f"Part 1 result = {res}")

    # Part 2 challenge.
    valid_tickets = [tk for tk in nearby_tks if all(any((rule[0] <= item <= rule[1] or
                                                         (rule[2] <= item <= rule[3]))
                                                        for rule in rules.values()) for item in tk)]

    temp_arr = np.array(valid_tickets)
    field_candidates = {col: [field for field in rules if
                              all(((rules[field][0] <= val <= rules[field][1]) or
                                   (rules[field][2] <= val <= rules[field][3])) for val in temp_arr[:, col])]
                        for col in range(temp_arr.shape[1])}

    final_map = find_rule(field_candidates)
    good_col_idx = [col for col, field in final_map.items() if field.startswith("departure")]
    logging.info(f"Result {np.prod(list(me[item] for item in good_col_idx))}")
