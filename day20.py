"""Solves day 20 challenge.
--- Day 20: Jurassic Jigsaw ---
The high-speed train leaves the forest and quickly carries you south. You can even see a desert in the distance! Since
you have some spare time, you might as well see if there was anything interesting in the image the Mythical Information
Bureau satellite captured.

After decoding the satellite messages, you discover that the data actually contains many small images created by the
satellite's camera array. The camera array consists of many cameras; rather than produce a single square image, they
produce many smaller square image tiles that need to be reassembled back into a single image.

Each camera in the camera array returns a single monochrome image tile with a random unique ID number. The tiles (your
puzzle input) arrived in a random order.

Worse yet, the camera array appears to be malfunctioning: each image tile has been rotated and flipped to a random
orientation. Your first task is to reassemble the original image by orienting the tiles so they fit together.

To show how the tiles should be reassembled, each tile's image data includes a border that should line up exactly with
its adjacent tiles. All tiles have this border, and the border lines up exactly when the tiles are both oriented
correctly. Tiles at the edge of the image also have this border, but the outermost edges won't line up with any other
tiles.

For example, suppose you have the following nine tiles:

Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...
By rotating, flipping, and rearranging them, you can find a square arrangement that causes all adjacent borders to line
up:

#...##.#.. ..###..### #.#.#####.
..#.#..#.# ###...#.#. .#..######
.###....#. ..#....#.. ..#.......
###.##.##. .#.#.#..## ######....
.###.##### ##...#.### ####.#..#.
.##.#....# ##.##.###. .#...#.##.
#...###### ####.#...# #.#####.##
.....#..## #...##..#. ..#.###...
#.####...# ##..#..... ..#.......
#.##...##. ..##.#..#. ..#.###...

#.##...##. ..##.#..#. ..#.###...
##..#.##.. ..#..###.# ##.##....#
##.####... .#.####.#. ..#.###..#
####.#.#.. ...#.##### ###.#..###
.#.####... ...##..##. .######.##
.##..##.#. ....#...## #.#.#.#...
....#..#.# #.#.#.##.# #.###.###.
..#.#..... .#.##.#..# #.###.##..
####.#.... .#..#.##.. .######...
...#.#.#.# ###.##.#.. .##...####

...#.#.#.# ###.##.#.. .##...####
..#.#.###. ..##.##.## #..#.##..#
..####.### ##.#...##. .#.#..#.##
#..#.#..#. ...#.#.#.. .####.###.
.#..####.# #..#.#.#.# ####.###..
.#####..## #####...#. .##....##.
##.##..#.. ..#...#... .####...#.
#.#.###... .##..##... .####.##.#
#...###... ..##...#.. ...#..####
..#.#....# ##.#.#.... ...##.....
For reference, the IDs of the above tiles are:

1951    2311    3079
2729    1427    2473
2971    1489    1171
To check that you've assembled the image correctly, multiply the IDs of the four corner tiles together. If you do this
with the assembled tiles from the example above, you get 1951 * 3079 * 2971 * 1171 = 20899048083289.

Assemble the tiles into an image. What do you get if you multiply together the IDs of the four corner tiles?

--- Part Two ---
Now, you're ready to check the image for sea monsters.

The borders of each tile are not part of the actual image; start by removing them.

In the example above, the tiles become:

.#.#..#. ##...#.# #..#####
###....# .#....#. .#......
##.##.## #.#.#..# #####...
###.#### #...#.## ###.#..#
##.#.... #.##.### #...#.##
...##### ###.#... .#####.#
....#..# ...##..# .#.###..
.####... #..#.... .#......

#..#.##. .#..###. #.##....
#.####.. #.####.# .#.###..
###.#.#. ..#.#### ##.#..##
#.####.. ..##..## ######.#
##..##.# ...#...# .#.#.#..
...#..#. .#.#.##. .###.###
.#.#.... #.##.#.. .###.##.
###.#... #..#.##. ######..

.#.#.### .##.##.# ..#.##..
.####.## #.#...## #.#..#.#
..#.#..# ..#.#.#. ####.###
#..####. ..#.#.#. ###.###.
#####..# ####...# ##....##
#.##..#. .#...#.. ####...#
.#.###.. ##..##.. ####.##.
...###.. .##...#. ..#..###
Remove the gaps to form the actual image:

.#.#..#.##...#.##..#####
###....#.#....#..#......
##.##.###.#.#..######...
###.#####...#.#####.#..#
##.#....#.##.####...#.##
...########.#....#####.#
....#..#...##..#.#.###..
.####...#..#.....#......
#..#.##..#..###.#.##....
#.####..#.####.#.#.###..
###.#.#...#.######.#..##
#.####....##..########.#
##..##.#...#...#.#.#.#..
...#..#..#.#.##..###.###
.#.#....#.##.#...###.##.
###.#...#..#.##.######..
.#.#.###.##.##.#..#.##..
.####.###.#...###.#..#.#
..#.#..#..#.#.#.####.###
#..####...#.#.#.###.###.
#####..#####...###....##
#.##..#..#...#..####...#
.#.###..##..##..####.##.
...###...##...#...#..###
Now, you're ready to search for sea monsters! Because your image is monochrome, a sea monster will look like this:

                  #
#    ##    ##    ###
 #  #  #  #  #  #
When looking for this pattern in the image, the spaces can be anything; only the # need to match. Also, you might need to rotate or flip your image before it's oriented correctly to find sea monsters. In the above image, after flipping and rotating it to the appropriate orientation, there are two sea monsters (marked with O):

.####...#####..#...###..
#####..#..#.#.####..#.#.
.#.#...#.###...#.##.O#..
#.O.##.OO#.#.OO.##.OOO##
..#O.#O#.O##O..O.#O##.##
...#.#..##.##...#..#..##
#.##.#..#.#..#..##.#.#..
.###.##.....#...###.#...
#.####.#.#....##.#..#.#.
##...#..#....#..#...####
..#.##...###..#.#####..#
....#.##.#.#####....#...
..##.##.###.....#.##..#.
#...#...###..####....##.
.#.##...#.##.#.#.###...#
#.###.#..####...##..#...
#.###...#.##...#.##O###.
.O##.#OO.###OO##..OOO##.
..O#.O..O..O.#O##O##.###
#.#..##.########..#..##.
#.#####..#.#...##..#....
#....##..#.#########..##
#...#.....#..##...###.##
#..###....##.#...##.##.#
Determine how rough the waters are in the sea monsters' habitat by counting the number of # that are not part of a sea monster. In the above example, the habitat's water roughness is 273.

How many # are not part of a sea monster?


"""
from pathlib import Path
from os.path import join
from absl import logging
import numpy as np
from typing import List
from typing import Dict
from typing import Tuple
from enum import Enum
from enum import auto
from itertools import product
from functools import reduce
from operator import mul


class Edge(Enum):
    L = 0
    B = 1
    R = 2
    T = 3



class RotationTransform(Enum):
    R0 = 0
    R90 = 1
    R180 = 2
    R270 = 3

class FlipTransform(Enum):
    FNo = auto
    FLR = auto()
    FUD = auto()


def parse_image(lines: List[str]) -> np.ndarray:
    return np.array([list(map(int, list(line.translate(str.maketrans('.#', '01'))))) for line in lines])

def transform_im(im: np.ndarray, rot_t: RotationTransform, flip_t: FlipTransform) -> np.ndarray:
    if rot_t == RotationTransform.R0:
        temp_im = im
    if rot_t == RotationTransform.R90:
        temp_im = np.rot90(im)
    if rot_t == RotationTransform.R180:
        temp_im = np.rot90(np.rot90(im))
    if rot_t == RotationTransform.R270:
        temp_im = np.rot90(np.rot90(np.rot90(im)))

    if flip_t == FlipTransform.FUD:
        temp_im = np.flipud(temp_im)
    if flip_t == FlipTransform.FLR:
        temp_im = np.fliplr(temp_im)
    return temp_im

def find_first_match(ref_im, query_im):
    """ Returns a transformed version of the query image to match with the reference image.

    (ref edge, query transformation type, query edge) or None. """
    def get_edge(im: np.ndarray, im_edge_type: Edge) -> np.ndarray:

        if im_edge_type == Edge.L:
            return im[:, 0]
        if im_edge_type == Edge.R:
            return im[:, -1]
        if im_edge_type == Edge.T:
            return im[0, :]
        if im_edge_type == Edge.B:
            return im[-1, :]

    # Check all edges of the reference
    for edge_type in Edge:
        edge = get_edge(ref_im, edge_type)
        for rot, flip in product(RotationTransform, FlipTransform):
            trans_q_im = transform_im(query_im, rot, flip)
            if edge_type == Edge.T:
                if np.all(edge == get_edge(trans_q_im, Edge.B)):
                    return [(rot, flip), edge_type]
            if edge_type == Edge.B:
                if np.all(edge == get_edge(trans_q_im, Edge.T)):
                    return [(rot, flip), edge_type]
            if edge_type == Edge.L:
                if np.all(edge == get_edge(trans_q_im, Edge.R)):
                    return [(rot, flip), edge_type]
            if edge_type == Edge.R:
                if np.all(edge == get_edge(trans_q_im, Edge.L)):
                    return [(rot, flip), edge_type]

    # Return a tuple of transformation to perform by the neighbor and the neighbor's edge.
    return None


def combine_image(adjecency_map: Dict[Tuple[int, Edge], Tuple[int, Tuple[RotationTransform, FlipTransform], Edge]],
                  all_tiles: np.ndarray) -> np.ndarray:

    def connection_candidate_tiles(used_ids):
        pot_ids = set()
        for used_id in used_ids:
            for v in adjecency_map[used_id]:
                pot_ids.add(v[0])
        return pot_ids

    def find_used_tile_connection(new_tile_id, used_ids):
        """Find the index of the tile that links to it."""
        neighbor_set = set(v[0] for v in adjecency_map[new_tile_id])
        return neighbor_set.intersection(used_ids).pop()

    def calculate_cand_idx(to_nei_edge, nei_idx) -> Tuple[int, int]:
        """Computes the index of the cand tile given the edge of its neighbors and the index of the neighbor."""
        if to_nei_edge == Edge.T:
            return nei_idx[0] - 1, nei_idx[1]
        if to_nei_edge == Edge.B:
            return nei_idx[0] + 1, nei_idx[1]
        if to_nei_edge == Edge.L:
            return nei_idx[0], nei_idx[1] - 1
        if to_nei_edge == Edge.R:
            return nei_idx[0], nei_idx[1] + 1

    def calculate_ref_edge_in_stitch_map(neigh_edge, ref_transforms) -> Edge:
        def ref_ref_edge_in_1_trans(neigh_edge, transform) -> Edge:
            rot_t, flip_t = transform
            out_edge = Edge((neigh_edge.value + rot_t.value) % 4)
            if flip_t == FlipTransform.FNo:
                return out_edge
            if flip_t == FlipTransform.FUD:
                if out_edge == Edge.T:
                    return out_edge.B
                if out_edge == Edge.B:
                    return out_edge.T
                else:
                    return out_edge
            if flip_t == FlipTransform.FLR:
                if out_edge == Edge.L:
                    return out_edge.R
                if out_edge == Edge.R:
                    return out_edge.L
                else:
                    return out_edge

        out_edge = neigh_edge
        for transform in ref_transforms:
            out_edge = ref_ref_edge_in_1_trans(out_edge, transform)
        return out_edge

    def calculated_stitched_tile(tile, transforms):
        output = tile
        for transform in transforms:
            output = transform_im(output, transform[0], transform[1])
        return output

    def calculate_stitched_map(tile_indices, transformed_tiles, truncate_boundary=True) -> np.ndarray:
        min_row_idx = min(v[0] for v in tile_indices.values())
        max_row_idx = max(v[0] for v in tile_indices.values())
        min_col_idx = min(v[1] for v in tile_indices.values())
        max_col_idx = max(v[1] for v in tile_indices.values())
        tile_size = 8 if truncate_boundary else 10
        num_rows = max_row_idx - min_row_idx + 1
        num_cols = max_col_idx - min_col_idx + 1
        out_map = np.zeros((num_rows * tile_size, num_cols * tile_size))
        for tile in transformed_tiles:
            row_idx, col_idx = tile_indices[tile]
            row_idx = (row_idx - min_row_idx) * tile_size
            col_idx = (col_idx - min_col_idx) * tile_size
            out_map[row_idx:row_idx + tile_size, col_idx:col_idx + tile_size] = transformed_tiles[tile][1:-1, 1:-1] \
                if truncate_boundary else transformed_tiles[tile]
        return out_map

    def stitch_tiles():
        tile_ids = list(adjecency_map.keys())
        used_ids = [tile_ids[0]]
        # A dictionary that denotes the transform used before stitching the tiles together.
        used_transforms = {tile_ids[0]: [(RotationTransform.R0, FlipTransform.FNo)]}
        used_indices = {tile_ids[0]: (0, 0)}
        transformed_tiles = {tile_ids[0]: all_tiles[tile_ids[0]]}
        connection_candidate_tiles(used_ids)

        while True:
            connection_candidates = connection_candidate_tiles(used_ids)
            to_add_candidates = connection_candidates.difference(used_ids)
            if len(to_add_candidates) == 0:
                break
            for cand in to_add_candidates:
                nei_id = find_used_tile_connection(cand, used_ids)
                # Extract all information from the neighbor about cand so that it knows how to transform.
                temp = list((v[1], v[2]) for v in adjecency_map[nei_id] if v[0] == cand)[0]

                # Transformation needed by the cand to match the un-transformed neighbor
                cand_trans = temp[0]

                # Find the transformation done on the neighbor and apply it to the current image.
                used_nei_trans = used_transforms[nei_id]

                # Find the transformation that needs to be done on the candidate so that it can be stitched.
                used_transforms[cand] = [cand_trans, *used_nei_trans]

                transformed_tiles[cand] = calculated_stitched_tile(all_tiles[cand], used_transforms[cand])

                # Calculate the edge used by the neighbor to connect to this tile.
                to_cand_edge = calculate_ref_edge_in_stitch_map(temp[1], used_nei_trans)
                used_indices[cand] = calculate_cand_idx(to_cand_edge, used_indices[nei_id])
                used_ids.append(cand)

                # Compute the index of the tile in the new map.

        logging.info("Stitching all_tiles tile locations...")
        return calculate_stitched_map(used_indices, transformed_tiles)

    return stitch_tiles()


def count_monster(monster, stitched):
    monster_shape = monster.shape
    stitched_shape = stitched.shape
    count = 0
    total_white_count = np.sum(monster)
    for y, x in product(range(stitched_shape[0] - monster_shape[0] + 1), range(stitched_shape[1] - monster_shape[1])):
        if np.sum(monster * stitched[y : y + monster_shape[0],x : x + monster_shape[1]]) == total_white_count:
            # Mask all the 1 region of the stitch of we find a match
            stitched[y: y + monster_shape[0], x: x + monster_shape[1]] *= (1 - monster)
            count += 1
    if count > 0:
        logging.info(f"Part 2 result = {int(np.sum(stitched))}")

    return count


def day20():
    data = [line[:-1] for line in open(join(Path(__file__).parent, "data", "day20_input.txt"), "r").readlines()]
    num_images = (len(data) + 1) // 12
    all_tiles: Dict[int, np.ndarray] = {}
    for tile in range(num_images):
        tile_id = int(data[tile * 12].split(' ')[1][:-1])
        tile_val = parse_image(data[tile * 12 + 1: tile * 12 + 11])
        all_tiles[tile_id] = tile_val
    tile_ids = list(all_tiles.keys())
    adjecency_map: Dict[int, Tuple[int, np.ndarray, Edge]] = {}

    neighbor_count = {}
    for tile_id in tile_ids:
        logging.info(f"Finding match for {tile_id}")
        adjecency_map[tile_id] = []
        for tile_id_other in tile_ids:
            if tile_id_other != tile_id:
                temp_res = find_first_match(all_tiles[tile_id], all_tiles[tile_id_other])
                if temp_res is not None:
                    adjecency_map[tile_id].append([tile_id_other, *temp_res])
        count = len(adjecency_map[tile_id])
        neighbor_count[tile_id] = count
    logging.info(f"Part 1 result = {reduce(mul, (k for k in neighbor_count if neighbor_count[k] == 2))}")

    logging.info("Stitching the patches together")
    out_im = combine_image(adjecency_map, all_tiles)
    import matplotlib.pyplot as plt
    plt.imshow(out_im)
    plt.show()
    monters_pattern = ['                  # ',
                       '#    ##    ##    ###',
                       ' #  #  #  #  #  #   ']
    monster = np.array([list(map(int, list(line.translate(str.maketrans(' #', '01'))))) for line in monters_pattern])
    for rot_t, flip_t in product(RotationTransform, FlipTransform):
        temp_im = transform_im(out_im, rot_t, flip_t)
        count = count_monster(monster, temp_im)
        logging.info(f"Transform {rot_t}, flip = {flip_t}, count = {count}")



