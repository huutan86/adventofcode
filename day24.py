"""Solves day 24th challenge.

--- Day 24: Lobby Layout ---
Your raft makes it to the tropical island; it turns out that the small crab was an excellent navigator. You make your
way to the resort.

As you enter the lobby, you discover a small problem: the floor is being renovated. You can't even reach the check-in
desk until they've finished installing the new tile floor.

The tiles are all hexagonal; they need to be arranged in a hex grid with a very specific color pattern. Not in the mood
to wait, you offer to help figure out the pattern.

The tiles are all white on one side and black on the other. They start with the white side facing up. The lobby is large
 enough to fit whatever pattern might need to appear there.

A member of the renovation crew gives you a list of the tiles that need to be flipped over (your puzzle input). Each
line in the list identifies a single tile that needs to be flipped by giving a series of steps starting from a reference
tile in the very center of the room. (Every line starts from the same reference tile.)

Because the tiles are hexagonal, every tile has six neighbors: east, southeast, southwest, west, northwest, and northeast.
These directions are given in your list, respectively, as e, se, sw, w, nw, and ne. A tile is identified by a series of
these directions with no delimiters; for example, esenee identifies the tile you land on if you start at the reference
tile and then move one tile east, one tile southeast, one tile northeast, and one tile east.

Each time a tile is identified, it flips from white to black or from black to white. Tiles might be flipped more than
once. For example, a line like esew flips a tile immediately adjacent to the reference tile, and a line like nwwswee
flips the reference tile itself.

Here is a larger example:

sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew
In the above example, 10 tiles are flipped once (to black), and 5 more are flipped twice (to black, then back to white).
 After all of these instructions have been followed, a total of 10 tiles are black.

Go through the renovation crew's list and determine which tiles they need to flip. After all of the instructions have
been followed, how many tiles are left with the black side up?

--- Part Two ---
The tile floor in the lobby is meant to be a living art exhibit. Every day, the tiles are all flipped according to the
following rules:

Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
Here, tiles immediately adjacent means the six tiles directly touching the tile in question.

The rules are applied simultaneously to every tile; put another way, it is first determined which tiles need to be
flipped, then they are all flipped at the same time.

In the above example, the number of black tiles that are facing up after the given number of days has passed is as
follows:

Day 1: 15
Day 2: 12
Day 3: 25
Day 4: 14
Day 5: 23
Day 6: 28
Day 7: 41
Day 8: 37
Day 9: 49
Day 10: 37

Day 20: 132
Day 30: 259
Day 40: 406
Day 50: 566
Day 60: 788
Day 70: 1106
Day 80: 1373
Day 90: 1844
Day 100: 2208
After executing this process a total of 100 times, there would be 2208 black tiles facing up.

How many tiles will be black after 100 days?
"""
from pathlib import Path
from os.path import join
from typing import List
from typing import Tuple
import re
from absl import logging


neighbor_offsets = [((2, 0), (0, 0)),
                    ((1, 0), (0, -1)),
                    ((-1, 0), (0, -1)),
                    ((-2, 0), (0, 0)),
                    ((-1, 0), (0, 1)),
                    ((1, 0), (0, 1))]

expand_offsets = neighbor_offsets.copy()
expand_offsets.append(((0, 0), (0, 0)))  # Include itself.


def parse_line(line: str) -> List[str]:
    out = re.findall("(e|se|sw|w|nw|ne)", line)
    return out


def compute_coord(tiles: List[str]) -> Tuple[float, float]:
    def diff_comp(cmd) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        # For each coordinate, return a tuple of (a, b) such that the coordinate value is a * 0.5 + b * sqrt(3) / 2
        # This avoids the rounding error.
        if cmd == 'e':
            return (2, 0), (0, 0)
        if cmd == 'se':
            return (1, 0), (0, -1)
        if cmd == 'sw':
            return (-1, 0), (0, -1)
        if cmd == 'w':
            return (-2, 0), (0, 0)
        if cmd == 'nw':
            return (-1, 0), (0, 1)
        if cmd == 'ne':
            return (1, 0), (0, 1)
    pos = (0, 0), (0, 0)
    for tile in tiles:
        (x1, x2), (y1, y2) = diff_comp(tile)
        pos = (pos[0][0] + x1, pos[0][1] + x2), (pos[1][0] + y1, pos[1][1] + y2)
    return pos


def black_neighbor_count(pos: Tuple[Tuple[int, int], Tuple[int, int]], tile_statuses) -> int:
    count = 0
    for offset in neighbor_offsets:
        (x1, x2), (y1, y2) = offset
        nei_coord = (pos[0][0] + x1, pos[0][1] + x2), (pos[1][0] + y1, pos[1][1] + y2)
        if nei_coord not in tile_statuses:
            continue
        elif tile_statuses[nei_coord] == 1:
            count += 1
    return count


def day24():
    data = [line[:-1] for line in open(join(Path(__file__).parent, "data", "day24_input.txt"), "r").readlines()]
    tiles = list(map(parse_line, data))
    tile_coords = [compute_coord(tile) for tile in tiles]

    # -- Part 1 --
    seen = {}
    for coord in tile_coords:
        if coord not in seen:
            seen[coord] = 1  # 0 = white, 1 = black
        else:
            seen[coord] = 1 - seen[coord]
    logging.info(f"Part 1 res = {sum(item == 1 for item in seen.values())}")

    # -- Part 2 --
    total_days = 100
    day = 0
    while day < total_days:
        to_update = {}
        visited = set()
        for tile in seen:
            for offset in expand_offsets:
                (x1, x2), (y1, y2) = offset
                nei_coord = (tile[0][0] + x1, tile[0][1] + x2), (tile[1][0] + y1, tile[1][1] + y2)
                if nei_coord not in visited:
                    # Caching it to save some time!

                    if nei_coord not in seen:
                        # This is a white tile.
                        if black_neighbor_count(nei_coord, seen) == 2:
                            to_update[nei_coord] = 1
                        else:
                            to_update[nei_coord] = 0

                    else:
                        if seen[nei_coord] == 0 and black_neighbor_count(nei_coord, seen) == 2:
                            # White coord with two black neighbors
                            to_update[nei_coord] = 1
                        if seen[nei_coord] == 1:
                            # Black coord with 0 or more than 2 black neighbors.
                            if black_neighbor_count(nei_coord, seen) == 0 or black_neighbor_count(nei_coord, seen) > 2:
                                to_update[nei_coord] = 0
                    visited.add(nei_coord)
                else:
                    pass

        # Update
        for item in to_update:
            seen[item] = to_update[item]
        day += 1
    logging.info(f"Day = {day}, part2  res = {sum(item == 1 for item in seen.values())}")

